# README #

SWINg!
By The Bay in Bogmalo, Goa
BAR | GUESTHOUSE | BISTRO

Located near Goa Airport, SWINg! By The Bay is a 50-seater bar & bistro that exists to please. This casual all-day-dining restaurant packs a punch that ranges from showcasing Goa In a Glass to serving traditional flavours of Kerala & America, from up-cycled décor accents to a laidback ambience, from on-demand karaoke to live music jams and from efficient service to an interactive vibe with warm hospitality. It is situated only a few steps away from the quaint Bogmalo Beach, which is a year-round destination & the nearest popular beach to Dabolim International Airport.

SWINg! exemplifies the unique era that it has been named after & pays homage to the individuality of expression that has come to be associated with this multi-faceted term. In keeping with this theme, the balanced menu features a comprehensive selection of specialties from the home country of swing & from the owner’s home state. This is where an American Diner meets a Kerala Rice house & where freshly prepared wholesome food stays true to the roots of both distinctive cuisines. 

The specialty chefs at SWINg! have a strong personal repertoire which has been further honed through training sessions that are held by expert malyali cooks. Vegetarians must try the Ullivada or Kappa Ularthiyathu for starters followed by the Spinach Dal fry & mix veg Korma. Preparations like the Kozhi Chettinad, beef fry, meen peera & chemmeen fry also make for excellent appetizers. Popular options like Nadan curry, Moilee & traditional kerala curry are made from scratch & cooked to perfection. The quintessential stews served with appams or Kerala rice are among the most popular meal choices here.

https://swingbythebay.com